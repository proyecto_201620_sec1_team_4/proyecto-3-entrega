package mundo;

import estructuras.ListaEncadenada;
import estructuras.SeparateChainingHashST;

public class Aerolinea {
	
	private String nombre;
	private double valorMinuto;
	private int tSMax;
	private SeparateChainingHashST<Integer, ListaEncadenada<Vuelo>> vuelos;
	
	public Aerolinea (String nombre, double valorMinuto, int tsMax){
		this.nombre = nombre;
		this.valorMinuto = valorMinuto;
		this.tSMax = tsMax;
		vuelos = new SeparateChainingHashST<Integer, ListaEncadenada<Vuelo>>();
	}
	
	public String darNombre () {
		return nombre;
	}
	
	public double darValorMinuto ( ){
		return valorMinuto;
	}
	
	public int darSillasMax ( ){
		return tSMax;
	}
	
	public SeparateChainingHashST<Integer, ListaEncadenada<Vuelo>> darVuelos ( ){
		return vuelos;
	}
	public void setValorMinuto(double valorMinuto) {
		this.valorMinuto = valorMinuto;
	}

	
	
	public void setVuelos (SeparateChainingHashST<Integer, ListaEncadenada<Vuelo>> vuelos){
		this.vuelos = vuelos;
	}
	
	
}
