package mundo;


public class Dia {
	
	private int dia;
	private Hora[] horas;
	
	public Dia (int dia )
	{
		this.dia = dia;
		horas = new Hora[24];
		for (int i = 0; i < horas.length; i++){
			horas[i] = new Hora(i);
		}
		
	}
	
	public int darDia ( )
	{
		return dia;
	}
	@SuppressWarnings("deprecation")
	public void agregarVuelo( Vuelo vuelo )
	{
		horas[vuelo.darHoraSalida().getHours()].agregarVuelo(vuelo);;
	}
	public Hora[] darHoras()
	{
		return horas;
	}
	
}
