package mundo;

import estructuras.BST;

public class Hora {
	private int hora;
	private BST<Vuelo, Integer> vuelos;
	
	public Hora(int hora)
	{
		this.hora = hora;
		vuelos = new BST<Vuelo, Integer>();
	}
	
	public void agregarVuelo( Vuelo vuelo )
	{
		vuelos.put(vuelo, vuelo.darNumeroVuelo());
	}
	
	public int darHora ( ){
		return hora;
	}
	public BST<Vuelo, Integer> darVuelos()
	{
		return vuelos;
	}
	
}
