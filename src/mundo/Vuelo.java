package mundo;

import java.sql.Time;

public class Vuelo implements Comparable <Vuelo>
{
	
	private Aerolinea aerolinea;
	private int numeroVuelo;
	private Time horaSalida;
	private Time horaLlegada;
	private String tipoEquipo;
	private int numeroSillas;
	private boolean[] operacion;
	private Ciudad origen;
	private Ciudad destino;
	private boolean tipoVuelo;
	private double[] tarifa;
	
	/**
	 * 
	 * @param aerolinea
	 * @param numeroVuelo
	 * @param origen
	 * @param destino
	 * @param horaSalida
	 * @param horaLlegada
	 * @param tipoEquipo
	 * @param numeroSillas
	 * @param operacion
	 * @param tipoVuelo
	 */
	public Vuelo (Aerolinea aerolinea, int numeroVuelo, Ciudad origen, Ciudad destino, Time horaSalida, Time horaLlegada, 
			String tipoEquipo, int numeroSillas, boolean[] operacion, boolean tipoVuelo )
	{
		this.aerolinea = aerolinea; 
		this.numeroVuelo = numeroVuelo;
		this.horaSalida = horaSalida;
		this.horaLlegada = horaLlegada;
		this.tipoEquipo = tipoEquipo;
		this.numeroSillas = numeroSillas;
		this.operacion = operacion;
		this.origen = origen;
		this.destino = destino;
		this.tipoVuelo = tipoVuelo;
		tarifa = new double [2];
	}
	
	public Aerolinea darAerolinea ( ) {
		return aerolinea;
	}
	
	public int darNumeroVuelo ( ) { 
		return numeroVuelo;
	
	}
	public Time darHoraSalida ( ) { 
		return horaSalida;
	}
	
	public Time darHoraLlegada ( ) { 
		return horaLlegada;
	}
	
	public String darTipoEquipo ( ) {
		return tipoEquipo;
	}
	
	public int darNumeroSillas ( ) { 
		return numeroSillas;
	}
	
	public boolean[] darOperacion ( ) {
		return operacion;
	}
	
	public Ciudad darCiudadOrigen ( ) { 
		return origen;
	}
	
	public Ciudad darCiudadDestino ( ) { 
		return destino;
	}
	
	public boolean darTipoVuelo ( ){
		return tipoVuelo;
	}
	
	public double[] darTarifas ( ){
		return tarifa;
	}
	
	@SuppressWarnings("deprecation")
	public void calcularTarifa ( ){
		
		if (numeroVuelo == 1008){
			boolean p = false;
		}
		
	
		double TM = this.aerolinea.darValorMinuto();
		int TSMax = this.aerolinea.darSillasMax();
		double TD = -1;
		
		int horasLlegadaMin = horaLlegada.getHours()*60;
		int horasSalidaMin = horaSalida.getHours()*60;
		
		if (horasLlegadaMin < horasSalidaMin){
			int duracionHoras = ((24 - horaSalida.getHours())*60) + horasLlegadaMin;
			int duracionMin = (59 - horaSalida.getMinutes()) + horaLlegada.getMinutes();
			TD = duracionHoras + duracionMin;
		} else {
			int duracionHoras = horaLlegada.getHours()*60 - horaSalida.getHours()*60;
			int duracionMin = horaLlegada.getMinutes() - horaSalida.getMinutes();
			TD = duracionHoras + duracionMin;
		}
		
		boolean ya = false; int i = 0;
		
		while (!ya && i < 4){
			if (operacion[i]){
				tarifa[0] = (TM*(TSMax/numeroSillas)*TD);
				ya = true;
			} else {
				tarifa[0] = 0.0;
				i ++;
			}
		}
		
		ya = false; i = 4;
		while (!ya && i < operacion.length){
			if (operacion[i]){
				tarifa[1] = (TM*(TSMax/numeroSillas)*TD*1.3);
				ya = true;
			} else {
				tarifa[1] = 0.0;
				i ++;
			}
		}
	}
	
	public int compareTo(Vuelo arg0)
	{
		return horaSalida.compareTo(arg0.darHoraSalida());
	}

	public double darTarifa(int dia) 
	{
		if( dia < 4 )
		{
			if (operacion[dia]){
				return tarifa[0];
			} else {
				return 0;
			}
		} else {
			if (operacion[dia]){
				return tarifa[1];
			} else {
				return 0;
			}
		}
		
	}
}
