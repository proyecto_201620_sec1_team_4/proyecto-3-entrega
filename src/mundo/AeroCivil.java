package mundo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;

import estructuras.BST;
import estructuras.Cola;
import estructuras.DijkstraSP;
import estructuras.DirectedEdge;
import estructuras.Edge;
import estructuras.EdgeWeightedDigraph;
import estructuras.GrafoDirigido;
import estructuras.Kosaraju;
import estructuras.ListaEncadenada;
import estructuras.PrimMST;
import estructuras.SeparateChainingHashST;
import importar.Datos;
import importar.ImportarDatos;

public class AeroCivil {

	/**
	 * grafo con solo las ciudades no tiene en cuenta la cantidad de vuelos ni la aerolinea
	 */
	private GrafoDirigido grafoConexionesCiudadesSinAerolinea;

	/**
	 * Hash con los grafos de cada aerolinea sin tener en cuenta horas de vuelo
	 */
	private SeparateChainingHashST<String, GrafoDirigido> hashGrafoConexionesCiudadesConAerolinea;
	/**
	 * Hash que recibe el nombre de una aerolinea y devuelve un arbol que contiene las ciudades que cubre dicha aerolinea
	 */
	private SeparateChainingHashST<String, BST<String, Boolean>> hashConexionesAerolinea;
	private BST<String, Ciudad> ciudades;
	private SeparateChainingHashST<String , Aerolinea> aerolineas;
	private Cola<Vuelo> ultimosVuelosAgregados;
	private int cantidadCiudades;
	@SuppressWarnings("unused")
	private int cantidadAerolineas;
	private ListaEncadenada<String> nombreAerolineas;

	public AeroCivil ( )
	{
		idCiudades = new SeparateChainingHashST<String, Integer>();
		cantidadCiudades = 0;
		cantidadAerolineas = 0;
		grafoConexionesCiudadesSinAerolinea = new GrafoDirigido();
		hashGrafoConexionesCiudadesConAerolinea = new SeparateChainingHashST<String, GrafoDirigido>();
		hashConexionesAerolinea = new SeparateChainingHashST<String, BST<String, Boolean>> ();
		nombreAerolineas = new ListaEncadenada<String>();
	}



	public void crearCatalogoDeVuelos (String RUTA)
	{
		ciudades = new BST<String, Ciudad>();
		aerolineas = new SeparateChainingHashST<String , Aerolinea>();
		ultimosVuelosAgregados = new Cola<Vuelo>();

		ImportarDatos importar = new ImportarDatos(RUTA);
		SeparateChainingHashST<String, ListaEncadenada<Datos>> datosVuelos = importar.darDatos().darDatos();
		ListaEncadenada<Datos> datosAerolineas = importar.darDatos().darDatosAerolineas();

		for (Datos dato: datosAerolineas){
			agregarAerolinea(dato.darAerolinea(),dato.darValorMinuto(),dato.darSillasMax());
			Aerolinea actual = aerolineas.get(dato.darAerolinea());		
			ListaEncadenada<Datos> lista = datosVuelos.get(actual.darNombre());
			for (Datos a:lista){
				agregarVuelo(a.darAerolinea(), a.darNumeroVuelo(), a.darOrigen(), a.darDestino(), a.darHoraSalida(), a.darHoraLlegada(), a.darTipoAvion(), a.darNumeroSillas(), a.darDiasVuelo(), a.darTipoVuelo());
			}
		}
		imprimirCatalogoVuelos();	
	}




	public void crearCatalogoDeVuelosVacio ( ){
		ciudades = new BST<String, Ciudad>();
		aerolineas = new SeparateChainingHashST<String , Aerolinea>();
		ultimosVuelosAgregados = new Cola<Vuelo>();
	}

	/**
	 * 
	 * @param nuevaAerolinea
	 * @param valorMinuto
	 * @param tSMax
	 * @return false si ya existe la aerolinea
	 */
	public boolean agregarAerolinea (String nombre, double valorMinuto, int tsMax)
	{
		if(!aerolineas.contains( nombre ) )
		{
			Aerolinea nueva = new Aerolinea(nombre, valorMinuto, tsMax);
			aerolineas.put(nombre, nueva);
			cantidadAerolineas++;
			nombreAerolineas.anadirUltimo(nombre);
			return true;
		}
		return false;

	}

	public void eliminarAerolinea (String aerolinea){
		
		try {
			
			if (aerolineas.contains(aerolinea)){
				aerolineas.delete(aerolinea);
			} else {
				throw new Exception("La aerolinea no existe");
			}
			
		} catch (Exception e){
			e.getMessage();
		}

	}

	/**
	 * 
	 * @param aerolinea
	 * @param numeroVuelo
	 * @param origen
	 * @param destino
	 * @param horaSalida
	 * @param horaLlegada
	 * @param avion
	 * @param numSillas
	 * @param dias
	 * @param tipoVuelo
	 * @return
	 * @throws Exception
	 */
	private SeparateChainingHashST<String, Integer> idCiudades = new SeparateChainingHashST<String, Integer>();
	private SeparateChainingHashST<Integer, String> reverseIdCiudades = new SeparateChainingHashST<Integer, String>();
	public void agregarVuelo (String aerolinea, int numeroVuelo, String origen, String destino, Time horaSalida, Time horaLlegada, String tipoEquipo, int numeroSillas, boolean[] operacion, boolean tipoVuelo)
	{
		boolean seDebeAgregar = true;
		boolean existeCiudadDeOrigen = agregarCiudad(origen);
		boolean existeCiudadDestino=agregarCiudad(destino);	
		Ciudad ciudadDeOrigen = ciudades.get(origen);
		Ciudad ciudadDeDestino = ciudades.get(destino);

		Vuelo nuevo = new Vuelo(aerolineas.get(aerolinea), numeroVuelo, ciudadDeOrigen , ciudadDeDestino, horaSalida, horaLlegada, tipoEquipo, numeroSillas, operacion, tipoVuelo);
		int dia = 0;
		//se agrega al arreglo de cada ciudad
		for (boolean b : operacion) 
		{
			if( b )
			{
				ciudadDeOrigen.agregarVuelo(nuevo, dia);
			}
			dia++;
		}
		
		// Agrega la aerolinea a la ciudad de origen.
		ciudadDeOrigen.agregarAerolinea(aerolineas.get(aerolinea));
		ciudadDeOrigen.agregarVueloCiudad(aerolineas.get(aerolinea), nuevo);
		ciudadDeDestino.agregarAerolinea(aerolineas.get(aerolinea));
				
		//Agrega el nuevo vuelo a una cola para posteriormente calcular su tarifa
		ultimosVuelosAgregados.enqueue(nuevo);		
		
		//nacional
		//se agrega al grafo general de conexiones
		int idCiudadDestino = idCiudades.get(destino);
		int idCiudadDeOrigen = idCiudades.get(origen);
		if( nuevo.darTipoVuelo())
		{
			if(  existeCiudadDeOrigen && existeCiudadDestino )
			{
				Iterable<Integer> adya =  grafoConexionesCiudadesSinAerolinea.adj(idCiudadDeOrigen);
				Iterator<Integer> a = adya.iterator();
				while( a.hasNext() )
				{
					int vueloAdj = a.next();
					if( vueloAdj == idCiudadDestino )
					{
						seDebeAgregar = false;
					}

				}
				if( seDebeAgregar )
				{
					grafoConexionesCiudadesSinAerolinea.addEdge(idCiudadDeOrigen, idCiudadDestino);
				}
			}
			else
			{
				grafoConexionesCiudadesSinAerolinea.addEdge(idCiudadDeOrigen, idCiudadDestino);
			}


			//se agrega al grafo referente a su respectiva aerolinea
			if( !hashConexionesAerolinea.contains(aerolinea))
			{
				BST<String, Boolean> catalogoPorAero = new BST<String, Boolean>();
				hashConexionesAerolinea.put(aerolinea, catalogoPorAero);
				GrafoDirigido grafoAerolinea = new GrafoDirigido();
				hashGrafoConexionesCiudadesConAerolinea.put(aerolinea, grafoAerolinea);


			}
			BST<String, Boolean> catalogoPorAerolinea = hashConexionesAerolinea.get(aerolinea);
			GrafoDirigido grafo = hashGrafoConexionesCiudadesConAerolinea.get(aerolinea);
			seDebeAgregar = true;
			if(  catalogoPorAerolinea.contains(origen)  && catalogoPorAerolinea.contains(destino) )
			{
				Iterable<Integer> adya =  grafo.adj(idCiudadDeOrigen);
				Iterator<Integer> a = adya.iterator();
				while( a.hasNext() )
				{
					int vueloAdj = a.next();
					if( vueloAdj == idCiudadDestino )
					{
						seDebeAgregar = false;
					}

				}
				if( seDebeAgregar )
				{
					grafo.addEdge(idCiudadDeOrigen, idCiudadDestino);
				}
			}
			else
			{
				grafo.addEdge(idCiudadDeOrigen, idCiudadDestino);
				catalogoPorAerolinea.put(origen, true);
				catalogoPorAerolinea.put(destino, true);
			}
		}
	}

	/**
	 * Método que agrega una ciudad
	 * Si no existe, la crea y retorna false
	 * Si ya existe, retorna true.
	 * @param key
	 */
	public boolean agregarCiudad (String key)
	{

		if (!ciudades.contains(key))
		{
			Ciudad nueva = new Ciudad(key);
			ciudades.put(key, nueva);
			idCiudades.put(key, cantidadCiudades);
			reverseIdCiudades.put( cantidadCiudades, key );
			cantidadCiudades++;
			return false;
		} 
		else {
			return true;
		}

	}

	/**
	 * Método que elimina una ciudad
	 * Retorna la ciudad eliminada.
	 * Manda excepción si no encuentra la ciudad a eliminar
	 * @param key
	 */
	public void eliminarCiudad (String key){

		try  {
			
			if (!ciudades.contains(key)){
				throw new Exception("La ciudad no existe");
			} else {
				ciudades.delete(key);
			}
		} catch (Exception e){
			e.getMessage();
		}

	}

	public void calcularActualizarTarifa ( ){

		while (!ultimosVuelosAgregados.isEmpty()){
			Vuelo actual = ultimosVuelosAgregados.dequeue();
			actual.calcularTarifa();
			DecimalFormat df = new DecimalFormat("0.00");
			System.out.println(actual.darAerolinea().darNombre() + " " + actual.darNumeroVuelo() + "\nTarifa L-J: $" + df.format(actual.darTarifas()[0]) + " USD\nTarifa V-D: $" + df.format(actual.darTarifas()[1]) + " USD\n");
		}

	}
	public void conjuntoDeCiudadesConectadasSinAerolineas ( )
	{
		kosaraju(grafoConexionesCiudadesSinAerolinea, null );
	}
	/**
	 * 
	 * @param grafo
	 * @param aero en la primera posicion en la segunda las ciudades visitadas
	 */
	@SuppressWarnings("unchecked")
	private void kosaraju( GrafoDirigido grafo, BST<String, Boolean> aero )
	{
		Kosaraju kos = new Kosaraju(grafo);
		int real = kos.count() - ( GrafoDirigido.CAPACITY - cantidadCiudades );
		Cola<Ciudad>[] arreglo = new Cola[ real ];
		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i] =  new Cola<Ciudad>();
		}

		int[] id = kos.darId();
		SeparateChainingHashST<Integer, Boolean> mango = new SeparateChainingHashST<Integer, Boolean>();
		int componentesReales = 0;
		for (int i = 1; i < cantidadCiudades; i++) 
		{
			int comp = id[ i ];
			String ciudad = reverseIdCiudades.get(i);
			Ciudad ciu = ciudades.get(ciudad);
			if( !ciu.esInternacional() && !mango.contains(comp))
			{
				mango.put(comp, true);
				componentesReales++;

			}
			arreglo[ comp ].enqueue(ciu);

		}
		//Arregla arreglos


		if( aero == null) System.out.println("Hay " + componentesReales +" componentes fuertemente conectados en el pais." );
		Cola<String> ciud = null;

		int nuev = 1;
		boolean temp = true;

		for (int i = 1; i < arreglo.length; i++) 
		{
			temp = false;
			ciud = new Cola<String>();
			Cola<Ciudad> col = arreglo[ i ];
			while( !col.isEmpty() )
			{
				Ciudad ciu = col.dequeue();
				boolean internacional = ciu.esInternacional();
				boolean esta = true;
				if( aero != null && !aero.contains(ciu.darNombreCiudad())) esta = false;
				if( !internacional && esta )
				{
					ciud.enqueue(ciu.darNombreCiudad());
					temp = true;
				}
			}
			if( temp && nuev <= componentesReales && !ciud.isEmpty()  )
			{

				System.out.print("Componente "+ nuev+":\t{");
				while( !ciud.isEmpty() )
				{
					String nombre = ciud.dequeue();
					if( ciud.isEmpty())
					{
						System.out.print(nombre);
					}
					else
					{
						System.out.print(nombre+", ");
					}

				}
				System.out.print("}");
				System.out.println();
				nuev++;
			}
		}
	}
	public void conjuntoDeCiudadesConectadasConAerolineas ( )
	{
		Iterator<String> itera =  nombreAerolineas.iterator();
		while( itera.hasNext() )
		{
			String aero = itera.next();
			System.out.println(aero);
			GrafoDirigido grafo = hashGrafoConexionesCiudadesConAerolinea.get(aero);
			if( grafo == null )
			{
				System.out.println("No hay vuelos nacionales");
			}
			else
			{
				kosaraju(grafo, hashConexionesAerolinea.get(aero));
			}
			System.out.println();
		}
	}
	//R9
	public void MSTVuelosNacionalesConTiempoSinAerolineas (String nombreCiudad) throws Exception
	{
		if (!ciudades.contains(nombreCiudad)) {
			throw new Exception("La ciudad no existe");
		}
		
		Ciudad ciudad = ciudades.get(nombreCiudad);
		EdgeWeightedDigraph mst = new EdgeWeightedDigraph();

		SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
		crearGrafo(mst, ciudad, true, 0, 0, 0, marked, true, null);
		PrimMST prim = new PrimMST(mst);

	}
	//R10
	public void MSTVuelosNacionalesConCostoConAerolineas (String nombreCiudad, String nombreAerolinea) throws Exception
	{ 
		if (!ciudades.contains(nombreCiudad)) {
			throw new Exception("La ciudad no existe");
		}
		if( !aerolineas.contains(nombreAerolinea))
		{
			throw new Exception( "La aerolinea no existe");
		}
		Ciudad ciudad = ciudades.get(nombreCiudad);
		Aerolinea aero = aerolineas.get(nombreAerolinea);
		EdgeWeightedDigraph mst = new EdgeWeightedDigraph();

		SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
		crearGrafo(mst, ciudad, false, 0, 0, 0, marked, true, aero);
	}
	//R11
	public void MSTVuelosTodosConTiempoSinAerolineas (String nombreCiudad, int dia) throws Exception
	{
		if (!ciudades.contains(nombreCiudad)) {
			throw new Exception("La ciudad no existe");
		}
		Ciudad ciudad = ciudades.get(nombreCiudad);
		EdgeWeightedDigraph mst = new EdgeWeightedDigraph();

		SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
		crearGrafo(mst, ciudad, true, dia, 0, 0, marked, false, null);
	}
	//R12
	@SuppressWarnings("unused")
	public void itinerarioDeCostoMinimoParaCadaAerolinea (String nombreCiudad, String ciudadDestino, int dia) throws Exception
	{
		if (!ciudades.contains(nombreCiudad)) {
			throw new Exception("La ciudad de partida no existe");
		}
		if (!ciudades.contains(ciudadDestino)) {
			throw new Exception("La ciudad de destino no existe");
		}
		Ciudad ciudad = ciudades.get(nombreCiudad);
		EdgeWeightedDigraph mst = new EdgeWeightedDigraph();
		Aerolinea aero = aerolineas.get("AVIANCA");
		SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
		crearGrafo(mst, ciudad, false, dia, 0, 0, marked, false, aero);
		DijkstraSP dij = new DijkstraSP(mst, idCiudades.get(nombreCiudad));
	}

	public void itinerarioCostoMinimoDiferentesAerolineas (String ciudadOrigen, String ciudadDestino, int diaPartida){
		
		try {
			
			if (!ciudades.contains(ciudadOrigen)) {
				throw new Exception("La ciudad de partida no existe");
			}
			if (!ciudades.contains(ciudadDestino)) {
				throw new Exception("La ciudad de destino no existe");
			}
			
			Ciudad ciudad = ciudades.get(ciudadOrigen);
			EdgeWeightedDigraph mst = new EdgeWeightedDigraph();
			
			SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
			crearGrafo(mst, ciudad, false, diaPartida, 0, 0, marked, false, null);
			DijkstraSP djk = new DijkstraSP(mst, idCiudades.get(ciudadOrigen));
			
		} catch (Exception e){
			e.getMessage();
		}
		

	}

	public void rutaConCostoMinimoPorCadaAerolinea (String nombreCiudad, String aerolinea){
		
		try {
			
			if (!ciudades.contains(nombreCiudad)) {
				throw new Exception("La ciudad de partida no existe");
			}

			Ciudad ciudad = ciudades.get(nombreCiudad);
			EdgeWeightedDigraph mst = new EdgeWeightedDigraph();
			Aerolinea aero = aerolineas.get(aerolinea);
			SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
			crearGrafo(mst, ciudad, true, 0, 0, 0, marked, false, aero);
			DijkstraSP dij = new DijkstraSP(mst, idCiudades.get(nombreCiudad));
			
		} catch (Exception e){
			e.getMessage();
		}

	}

	//true
	public void rutaConTiempoMinimoPorCadaAerolinea (String nombreCiudad, String aerolinea){
		try {
			
			if (!ciudades.contains(nombreCiudad)){
				throw new Exception ("La ciudad de partida no existe");
			}
			
			Ciudad ciudad = ciudades.get(nombreCiudad);
			EdgeWeightedDigraph di = new EdgeWeightedDigraph();
			Aerolinea aero = aerolineas.get(aerolinea);
			SeparateChainingHashST<Vuelo, Boolean> marked = new SeparateChainingHashST<Vuelo, Boolean>();
			crearGrafo(di, ciudad, true, 0, 0, 0, marked, true, aero);
			DijkstraSP min = new DijkstraSP(di, idCiudades.get(nombreCiudad));
			
			for (int i = 0; i < di.V(); i++){
				
				Iterator<DirectedEdge> iter = min.pathTo(i).iterator();
				
				while (iter.hasNext()){
					DirectedEdge edge = iter.next();
					
					String origen = reverseIdCiudades.get(edge.from());
					String destino = reverseIdCiudades.get(edge.to());
					
				}
			}
			
			
		} catch (Exception e){
			e.getMessage();
		}
	}
	
	/**
	 * 
	 * @param grafo El grafo a evaluar
	 * @param ciudad Ciudad de destino
	 * @param tiempo true si el costo es tiempo, false de lo contrario
	 * @param dia dia de partida
	 * @param hora hora de partida
 	 * @param minutos minutos de partida
	 * @param marked BST con los vuelos que ya fueron visitados
	 * @param nacional true si los vuelos son nacionales, false d elo contrario
	 * @param aero Aerolinea
	 */
	@SuppressWarnings("deprecation")
	public void crearGrafo (EdgeWeightedDigraph grafo, Ciudad ciudad, boolean tiempo, int dia, int hora, int minutos,
			SeparateChainingHashST<Vuelo, Boolean> marked, boolean nacional, Aerolinea aero)
	{
		Dia[] dias = ciudad.darDias();
		boolean terminoDias = false;
		int diaActual = dia;
		int cuantasSemanas = 0;
		while (!terminoDias ) //aumentar contador diaActual
		{
			Hora[] horas = dias[diaActual].darHoras();
			boolean terminoHoras = false;
			int horaActual = hora;
			int cuantosDias = 0;
			while (!terminoHoras )  //aumentar contador horaActual
			{
				BST<Vuelo, Integer> vuelos = horas[horaActual].darVuelos();
				if( vuelos.isEmpty() ) continue;
				Iterator<Vuelo> conjuntoDeVuelos =  vuelos.keys().iterator();
				while( conjuntoDeVuelos.hasNext() )
				{
					Vuelo vueloActual = conjuntoDeVuelos.next();
					if( (nacional && !vueloActual.darTipoVuelo()) || marked.contains(vueloActual) || vueloActual.darHoraSalida().getMinutes() < minutos || ( aero != null && (!aero.darNombre().equals(vueloActual.darAerolinea().darNombre())) ) )
					{
						continue;
					}
					else
					{
						//se consideran todas las restricciones
						double weight = calcularPeso(vueloActual, tiempo, diaActual);
						int diaRevisar = 0;
						if( vueloActual.darHoraLlegada().before(vueloActual.darHoraSalida()))
						{
							diaRevisar = diaActual++;
						}
						else
						{
							diaRevisar = diaActual;
						}
						DirectedEdge edge = new DirectedEdge(idCiudades.get(vueloActual.darCiudadOrigen().darNombreCiudad()),idCiudades.get(vueloActual.darCiudadDestino().darNombreCiudad()), weight);
						grafo.addEdge(edge);
						marked.put(vueloActual, true);
						crearGrafo(grafo, vueloActual.darCiudadDestino(), tiempo, diaRevisar, vueloActual.darHoraLlegada().getHours(),vueloActual.darHoraLlegada().getMinutes(), marked, nacional, aero);
					}


				}
				if( horaActual == hora )
				{
					if( cuantosDias == 0 )
						cuantosDias++;
					else
					{
						terminoHoras = true;
					}

				}
				else
				{
					if( horaActual == horas.length-1) horaActual = 0;
					else
					{
						horaActual++;
					}
					minutos = 0;
				}
			}
			///
			if( diaActual == dia )
			{
				if( cuantasSemanas == 0 )
				{
					cuantasSemanas++;
				}
				else
				{
					terminoDias = true;
				}

			}
			else
			{
				if( diaActual == dias.length-1) diaActual = 0;
				else
				{
					diaActual++;
				}
			}

		}
	}
	
	
	@SuppressWarnings("deprecation")
	private double calcularPeso( Vuelo vueloActual, boolean tiempo, int dia)
	{
		double weight = 0;
		int minutosSalida = vueloActual.darHoraSalida().getHours()*60 + vueloActual.darHoraSalida().getMinutes();
		int minutosLlegada = vueloActual.darHoraLlegada().getHours()*60 + vueloActual.darHoraLlegada().getMinutes();
		if( minutosLlegada < minutosSalida )
		{
			weight = minutosLlegada + (24*60) - minutosSalida;
		}
		else
		{
			weight = minutosLlegada - minutosSalida;
		}
		if( !tiempo )
		{
			//revisar si esta bien la tarifa
			weight*= vueloActual.darTarifa(dia);
		}
		return weight;
	}


	private void imprimirCatalogoVuelos ( ){
	// Crea los objetos necesarios para escribir el archivo de texto
	try {
		PrintWriter writer = new PrintWriter(new File("./data/CatalogoVuelos.txt"));
				
		Iterator<String> iterCiudades = ciudades.keys().iterator();
		Ciudad ciudadAnterior = null;
			
		while (iterCiudades.hasNext()){
			String aerolineaAnterior = null;
			String nombreCiudad = iterCiudades.next();	
			Ciudad ciudadActual = ciudades.get(nombreCiudad);
			BST<String, BST<Integer, ListaEncadenada<Vuelo>>> vuelosCiudad = ciudadActual.darAerolineasCiudad();
			Iterator<String> iterAerolineasCiudad = vuelosCiudad.keys().iterator();
			
			while (iterAerolineasCiudad.hasNext()){
				
				String aerolineaActual = iterAerolineasCiudad.next();			
				BST<Integer, ListaEncadenada<Vuelo>> arbolVuelosCiudad = vuelosCiudad.get(aerolineaActual);
				
				if (arbolVuelosCiudad.size() > 0){
					Iterator<Integer> iterIDVuelos = arbolVuelosCiudad.keys().iterator();
					
					Vuelo vueloAnt = null;
						
					while (iterIDVuelos.hasNext()){
							
						int id = iterIDVuelos.next();
						ListaEncadenada<Vuelo> vueloActual = arbolVuelosCiudad.get(id);
						Iterator<Vuelo> iterVuelos = vueloActual.iterator();
							
						int idAnt = -1;
										
						while (iterVuelos.hasNext()){
								
							Vuelo vuelo = iterVuelos.next();
							String tipo = ((vuelo.darTipoVuelo()) ? "Nacional" : "Internacional");
								
							if (ciudadAnterior == null || !ciudadActual.darNombreCiudad().equalsIgnoreCase(ciudadAnterior.darNombreCiudad())){
								String linea = ciudadActual.darNombreCiudad() + "\n";
								System.out.println(linea);
								writer.println(linea);
								ciudadAnterior = ciudadActual;
							}
								
							if (aerolineaAnterior == null || !aerolineaActual.equalsIgnoreCase(aerolineaAnterior)){
								String linea = aerolineaActual;
								System.out.println(linea);
								writer.println(linea);
								aerolineaAnterior = aerolineaActual;
							}
								
							if (vueloAnt != null){
								if (!vuelo.darCiudadDestino().darNombreCiudad().equalsIgnoreCase(vueloAnt.darCiudadDestino().darNombreCiudad())){
									String linea = "\nDestino: " + vuelo.darCiudadDestino().darNombreCiudad();
									System.out.println(linea);
									writer.println(linea);
								}
							} else {
								String linea = "Destino: " +  vuelo.darCiudadDestino().darNombreCiudad();
								System.out.println(linea);
								writer.println(linea);
							}
								
							String linea = ((id != idAnt) ? "# Vuelo: " + id + " Tipo de Vuelo: " + tipo : null);
							String enca = "Hora Salida  Hora Llegada  AviÛn  # Sillas  L M I J V S D";
							
							if (linea != null) {
								System.out.println(linea + "\n" + enca); 
								writer.println(linea);
								writer.println(enca);
							}
								
							String info = "";
							
							for (int i = 0; i < vuelo.darOperacion().length; i++){
								if (vuelo.darOperacion()[i]){
									info = info + "x ";
								} else {
									info = info + "  ";
								}
							}
								
							String horaSalida = vuelo.darHoraSalida().toString().substring(0, vuelo.darHoraSalida().toString().length() - 3);
							String horaLlegada = vuelo.darHoraLlegada().toString().substring(0, vuelo.darHoraLlegada().toString().length() - 3);
							String tipoAVion = ((vuelo.darTipoEquipo().length() < 4) ? " " + vuelo.darTipoEquipo() : vuelo.darTipoEquipo());
							String numSillas = ((vuelo.darNumeroSillas() < 100) ? " " + Integer.toString(vuelo.darNumeroSillas()) : Integer.toString(vuelo.darNumeroSillas()));
								
							String infoVuelo =  "   " + horaSalida + "        " + horaLlegada + "      " + tipoAVion + "     " + numSillas + "     " + info;
							System.out.println(infoVuelo);
							writer.println(infoVuelo);
								
							vueloAnt = vuelo;
							idAnt = vuelo.darNumeroVuelo();
						}
							
						if (!iterIDVuelos.hasNext()){
							System.out.println("");
							writer.println("");
						}
					}
				}
			}
		}
		writer.close();
		
		System.out.println("Se ha generado el archivo de texto con el catálogo de vuelos");
				
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}	
}
	}

