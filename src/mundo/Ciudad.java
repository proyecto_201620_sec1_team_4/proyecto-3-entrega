package mundo;

import java.util.Iterator;

import estructuras.BST;
import estructuras.ListaEncadenada;

public class Ciudad {

	private String nombreCiudad;
	private Dia[] dias;
	private BST<String, BST<Integer, ListaEncadenada<Vuelo>>> aerolineasCiudad;

	public Ciudad (String nombreCiudad)
	{
		this.nombreCiudad = nombreCiudad;
		dias = new Dia[7];
		for (int i = 0; i < dias.length; i++) {
			dias[i] = new Dia(i);

		}
		
		aerolineasCiudad = new BST<String, BST<Integer, ListaEncadenada<Vuelo>>> ( );
	}

	public void agregarVuelo( Vuelo vuelo, int dia )
	{
		Dia diaActual = dias[ dia ];
		diaActual.agregarVuelo(vuelo);

	}

	public String darNombreCiudad ( )
	{
		return nombreCiudad;
	}

	public Dia[] darDias()
	{
		return dias;
	}
	
	public boolean esInternacional()
	{
		for (int i = 0; i < dias.length; i++)
		{
			Dia diaAc = dias[i];
			Hora[] horas = diaAc.darHoras();
			for (int j = 0; j < horas.length; j++) 
			{
				BST<Vuelo, Integer> vuelos = horas[j].darVuelos();
				if( vuelos.isEmpty() ) continue;
				Iterator<Vuelo> conjuntoDeVuelos =  vuelos.keys().iterator();
				while( conjuntoDeVuelos.hasNext() )
				{
					Vuelo vueloActual = conjuntoDeVuelos.next();
					if( vueloActual.darTipoVuelo() ) return false;


				}
			}
		}
		return true;
	}
	
	public BST<String, BST<Integer, ListaEncadenada<Vuelo>>> darAerolineasCiudad ( ){
		return aerolineasCiudad;
	}
	
	/**
	 * Método que agrega una aerolinea que opera en dicha ciudad
	 * Su uso es para crear el catálogo de vuelos
	 * @param aerolinea
	 */
	public void agregarAerolinea (Aerolinea aerolinea){
		if (!aerolineasCiudad.contains(aerolinea.darNombre())){
			BST<Integer, ListaEncadenada<Vuelo>> vuelos = new BST<Integer, ListaEncadenada<Vuelo>> ( );
			aerolineasCiudad.put(aerolinea.darNombre(), vuelos);
		}
	}
	
	/**
	 * Método que agrega un vuelo a la ciudad sin distinguir su horario de operación
	 * Su uso es para crear el catálogo de vuelos.
	 */
	public void agregarVueloCiudad (Aerolinea aerolinea, Vuelo vuelo){
		
		try{
			
			if (aerolineasCiudad.contains(aerolinea.darNombre())){
				
				BST<Integer, ListaEncadenada<Vuelo>> arbol = aerolineasCiudad.get(aerolinea.darNombre());
				
				if (!arbol.contains(vuelo.darNumeroVuelo())){
					ListaEncadenada<Vuelo> lista = new ListaEncadenada<Vuelo>();
					lista.anadirUltimo(vuelo);
					arbol.put(vuelo.darNumeroVuelo(), lista);
				} else {
					arbol.get(vuelo.darNumeroVuelo()).anadirUltimo(vuelo);
				}
				
			} else{
				throw new Exception ("La aerolinea no existe");
			}
			
		} catch (Exception e){
			e.getMessage();
		}
		
	}
}
