package estructuras;

/**
 * Clase gen�rica que modela un Nodo de la lista encadenada
 */	
public class Node<T> {
	//-------------------------------------------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------------------------------------------
	/**
	 * Atributo que modela el objeto que se guarda en la lista
	 */
	private T item;
	/**
	 * Atributo que modela el siguiente objeto que se est� en la lista
	 */
	private Node<T> siguiente;

	//-------------------------------------------------------------------------------------------------
	//Constructor
	//-------------------------------------------------------------------------------------------------
	/**
	 * Crea un nuevo nodo
	 * @param nuevo
	 */
	public Node (T nuevo){
		item = nuevo;
		siguiente = null;
	}
	//-------------------------------------------------------------------------------------------------
	//M�todos
	//-------------------------------------------------------------------------------------------------
	/**
	 * Retorna el objeto que est� almacenado en la lista
	 * @return El objeto T actual
	 */
	public T getItem () {
		return item;
	}
	/**
	 * Retorna el siguiente nodo que est� almacenado en la lista
	 * @return el siguiente nodo, null si es el final de la lista
	 */
	public Node<T> getNext() {
		return siguiente;
	}
	/**
	 * Ingresa el nodo que entra por parametro despu�s del acutal nodo
	 * @param pSig nodo que 
	 */
	public void setNext(Node<T> pSig){
		siguiente = pSig;
	}
}
