package estructuras;

/**
 *  The {@code KosarajuSharirSCC} class represents a data type for 
 *  determining the strong components in a digraph.
 *  The <em>id</em> operation determines in which strong component
 *  a given vertex lies; the <em>areStronglyConnected</em> operation
 *  determines whether two vertices are in the same strong component;
 *  and the <em>count</em> operation determines the number of strong
 *  components.

 *  The <em>component identifier</em> of a component is one of the
 *  vertices in the strong component: two vertices have the same component
 *  identifier if and only if they are in the same strong component.

 *  <p>
 *  This implementation uses the Kosaraju-Sharir algorithm.
 *  The constructor takes time proportional to <em>V</em> + <em>E</em>
 *  (in the worst case),
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Afterwards, the <em>id</em>, <em>count</em>, and <em>areStronglyConnected</em>
 *  operations take constant time.
 *  For alternate implementations of the same API, see
 *  {@link TarjanSCC} and {@link GabowSCC}.
 *  <p>
 *  For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 * @param <K>
 */
public class Kosaraju {
    private boolean[] marked;     // marked[v] = has vertex v been visited?
    private int[] id;             // id[v] = id of strong component containing v
    private int count;            // number of strongly-connected components

    /**
     * Computes the strong components of the digraph {@code G}.
     * @param G the digraph
     */
    public Kosaraju(GrafoDirigido G) {

        // compute reverse postorder of reverse graph
        DepthFirstOrder dfs = new DepthFirstOrder(G.reverse());

        // run DFS on G, using reverse postorder to guide calculation
        marked = new boolean[G.V()];
        id = new int[G.V()];
        for (int v : dfs.reversePost()) {
            if (!marked[v]) {
                dfs(G, v);
                count++;
            }
        }

        // check that id[] gives strong components
        assert check(G);
    }

    // DFS on graph G
    private void dfs(GrafoDirigido G, int v) { 
        marked[v] = true;
        id[v] = count;
        for (int w : G.adj(v)) {
            if (!marked[w]) dfs(G, w);
        }
    }

    /**
     * Returns the number of strong components.
     * @return the number of strong components
     */
    public int count() {
        return count;
    }
    public int[] darId()
    {
    	return id;
    }

    /**
     * Are vertices {@code v} and {@code w} in the same strong component?
     * @param  v one vertex
     * @param  w the other vertex
     * @return {@code true} if vertices {@code v} and {@code w} are in the same
     *         strong component, and {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     * @throws IllegalArgumentException unless {@code 0 <= w < V}
     */
    public boolean stronglyConnected(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        return id[v] == id[w];
    }

    /**
     * Returns the component id of the strong component containing vertex {@code v}.
     * @param  v the vertex
     * @return the component id of the strong component containing vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public int id(int v) {
        validateVertex(v);
        return id[v];
    }

    // does the id[] array contain the strongly connected components?
    private boolean check(GrafoDirigido G) {
        TransitiveClosure tc = new TransitiveClosure(G);
        for (int v = 0; v < G.V(); v++) {
            for (int w = 0; w < G.V(); w++) {
                if (stronglyConnected(v, w) != (tc.reachable(v, w) && tc.reachable(w, v)))
                    return false;
            }
        }
        return true;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = marked.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
    
    /**
     *  The {@code TransitiveClosure} class represents a data type for 
     *  computing the transitive closure of a digraph.
     *  <p>
     *  This implementation runs depth-first search from each vertex.
     *  The constructor takes time proportional to <em>V</em>(<em>V</em> + <em>E</em>)
     *  (in the worst case) and uses space proportional to <em>V</em><sup>2</sup>,
     *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
     *  <p>
     *  For large digraphs, you may want to consider a more sophisticated algorithm.
     *  <a href = "http://www.cs.hut.fi/~enu/thesis.html">Nuutila</a> proposes two
     *  algorithm for the problem (based on strong components and an interval representation)
     *  that runs in <em>E</em> + <em>V</em> time on typical digraphs.
     *
     *  For additional documentation,
     *  see <a href="http://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
     *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
     *
     *  @author Robert Sedgewick
     *  @author Kevin Wayne
     */
    public class TransitiveClosure {
        private DirectedDFS[] tc;  // tc[v] = reachable from v

        /**
         * Computes the transitive closure of the digraph {@code G}.
         * @param G the digraph
         */
        public TransitiveClosure(GrafoDirigido G) {
            tc = new DirectedDFS[G.V()];
            for (int v = 0; v < G.V(); v++)
                tc[v] = new DirectedDFS(G, v);
        }

        /**
         * Is there a directed path from vertex {@code v} to vertex {@code w} in the digraph?
         * @param  v the source vertex
         * @param  w the target vertex
         * @return {@code true} if there is a directed path from {@code v} to {@code w},
         *         {@code false} otherwise
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         * @throws IllegalArgumentException unless {@code 0 <= w < V}
         */
        public boolean reachable(int v, int w) {
            validateVertex(v);
            validateVertex(w);
            return tc[v].marked(w);
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertex(int v) {
            int V = tc.length;
            if (v < 0 || v >= V)
                throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
        }
    }
    
    /**
     *  The {@code DirectedDFS} class represents a data type for 
     *  determining the vertices reachable from a given source vertex <em>s</em>
     *  (or set of source vertices) in a digraph. For versions that find the paths,
     *  see {@link DepthFirstDirectedPaths} and {@link BreadthFirstDirectedPaths}.
     *  <p>
     *  This implementation uses depth-first search.
     *  The constructor takes time proportional to <em>V</em> + <em>E</em>
     *  (in the worst case),
     *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
     *  <p>
     *  For additional documentation,
     *  see <a href="http://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
     *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
     *
     *  @author Robert Sedgewick
     *  @author Kevin Wayne
     */
    public class DirectedDFS{
        private boolean[] marked;  // marked[v] = true if v is reachable
                                   // from source (or sources)
        private int count;         // number of vertices reachable from s

        /**
         * Computes the vertices in digraph {@code G} that are
         * reachable from the source vertex {@code s}.
         * @param G the digraph
         * @param s the source vertex
         * @throws IllegalArgumentException unless {@code 0 <= s < V}
         */
        public DirectedDFS(GrafoDirigido G, int s) {
            marked = new boolean[G.V()];
            validateVertex(s);
            dfs(G, s);
        }

        /**
         * Computes the vertices in digraph {@code G} that are
         * connected to any of the source vertices {@code sources}.
         * @param G the graph
         * @param sources the source vertices
         * @throws IllegalArgumentException unless {@code 0 <= s < V}
         *         for each vertex {@code s} in {@code sources}
         */
        public DirectedDFS(GrafoDirigido G, Iterable<Integer> sources) {
            marked = new boolean[G.V()];
            validateVertices(sources);
            for (int v : sources) {
                if (!marked[v]) dfs(G, v);
            }
        }

        private void dfs(GrafoDirigido G, int v) { 
            count++;
            marked[v] = true;
            for (int w : G.adj(v)) {
                if (!marked[w]) dfs(G, w);
            }
        }

        /**
         * Is there a directed path from the source vertex (or any
         * of the source vertices) and vertex {@code v}?
         * @param  v the vertex
         * @return {@code true} if there is a directed path, {@code false} otherwise
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         */
        public boolean marked(int v) {
            validateVertex(v);
            return marked[v];
        }

        /**
         * Returns the number of vertices reachable from the source vertex
         * (or source vertices).
         * @return the number of vertices reachable from the source vertex
         *   (or source vertices)
         */
        public int count() {
            return count;
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertex(int v) {
            int V = marked.length;
            if (v < 0 || v >= V)
                throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertices(Iterable<Integer> vertices) {
            if (vertices == null) {
                throw new IllegalArgumentException("argument is null");
            }
            int V = marked.length;
            for (int v : vertices) {
                if (v < 0 || v >= V) {
                    throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
                }
            }
        }
    }
    
    /**
     *  The {@code DepthFirstOrder} class represents a data type for 
     *  determining depth-first search ordering of the vertices in a digraph
     *  or edge-weighted digraph, including preorder, postorder, and reverse postorder.
     *  <p>
     *  This implementation uses depth-first search.
     *  The constructor takes time proportional to <em>V</em> + <em>E</em>
     *  (in the worst case),
     *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
     *  Afterwards, the <em>preorder</em>, <em>postorder</em>, and <em>reverse postorder</em>
     *  operation takes take time proportional to <em>V</em>.
     *  <p>
     *  For additional documentation,
     *  see <a href="http://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
     *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
     *
     *  @author Robert Sedgewick
     *  @author Kevin Wayne
     */
    public class DepthFirstOrder{
        private boolean[] marked;          // marked[v] = has v been marked in dfs?
        private int[] pre;                 // pre[v]    = preorder  number of v
        private int[] post;                // post[v]   = postorder number of v
        private Cola<Integer> preorder;   // vertices in preorder
        private Cola<Integer> postorder;  // vertices in postorder
        private int preCounter;            // counter or preorder numbering
        private int postCounter;           // counter for postorder numbering

        /**
         * Determines a depth-first order for the digraph {@code G}.
         * @param G the digraph
         */
        public DepthFirstOrder(GrafoDirigido G) {
            pre    = new int[G.V()];
            post   = new int[G.V()];
            postorder = new Cola<Integer>();
            preorder  = new Cola<Integer>();
            marked    = new boolean[G.V()];
            for (int v = 0; v < G.V(); v++)
                if (!marked[v]) dfs(G, v);

            assert check();
        }

        /**
         * Determines a depth-first order for the edge-weighted digraph {@code G}.
         * @param G the edge-weighted digraph
         */
        public DepthFirstOrder(EdgeWeightedDigraph G) {
            pre    = new int[G.V()];
            post   = new int[G.V()];
            postorder = new Cola<Integer>();
            preorder  = new Cola<Integer>();
            marked    = new boolean[G.V()];
            for (int v = 0; v < G.V(); v++)
                if (!marked[v]) dfs(G, v);
        }

        // run DFS in digraph G from vertex v and compute preorder/postorder
        private void dfs(GrafoDirigido G, int v) {
            marked[v] = true;
            pre[v] = preCounter++;
            preorder.enqueue(v);
            for (int w : G.adj(v)) {
                if (!marked[w]) {
                    dfs(G, w);
                }
            }
            postorder.enqueue(v);
            post[v] = postCounter++;
        }

        // run DFS in edge-weighted digraph G from vertex v and compute preorder/postorder
        private void dfs(EdgeWeightedDigraph G, int v) {
            marked[v] = true;
            pre[v] = preCounter++;
            preorder.enqueue(v);
            for (DirectedEdge e : G.adj(v)) {
                int w = e.to();
                if (!marked[w]) {
                    dfs(G, w);
                }
            }
            postorder.enqueue(v);
            post[v] = postCounter++;
        }

        /**
         * Returns the preorder number of vertex {@code v}.
         * @param  v the vertex
         * @return the preorder number of vertex {@code v}
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         */
        public int pre(int v) {
            validateVertex(v);
            return pre[v];
        }

        /**
         * Returns the postorder number of vertex {@code v}.
         * @param  v the vertex
         * @return the postorder number of vertex {@code v}
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         */
        public int post(int v) {
            validateVertex(v);
            return post[v];
        }

        /**
         * Returns the vertices in postorder.
         * @return the vertices in postorder, as an iterable of vertices
         */
        public Iterable<Integer> post() {
            return postorder;
        }

        /**
         * Returns the vertices in preorder.
         * @return the vertices in preorder, as an iterable of vertices
         */
        public Iterable<Integer> pre() {
            return preorder;
        }

        /**
         * Returns the vertices in reverse postorder.
         * @return the vertices in reverse postorder, as an iterable of vertices
         */
        public Iterable<Integer> reversePost() {
            Pila<Integer> reverse = new Pila<Integer>();
            for (int v : postorder)
                reverse.push(v);
            return reverse;
        }


        // check that pre() and post() are consistent with pre(v) and post(v)
        private boolean check() {

            // check that post(v) is consistent with post()
            int r = 0;
            for (int v : post()) {
                if (post(v) != r) {
                	System.out.println("post(v) and post() inconsistent");
                    return false;
                }
                r++;
            }

            // check that pre(v) is consistent with pre()
            r = 0;
            for (int v : pre()) {
                if (pre(v) != r) {
                    System.out.println("pre(v) and pre() inconsistent");
                    return false;
                }
                r++;
            }

            return true;
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertex(int v) {
            int V = marked.length;
            if (v < 0 || v >= V)
                throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
        }
    }
}
