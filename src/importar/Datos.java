package importar;

import java.sql.Time;
import java.util.Iterator;

import estructuras.ListaEncadenada;
import estructuras.SeparateChainingHashST;

public class Datos {

	private String aerolinea;
	private int numeroVuelo;
	private String origen;
	private String destino;
	private Time horaSalida;
	private Time horaLlegada;
	private String avion;
	private int numSillas;
	private boolean[] dias;
	private boolean tipoVuelo;
	private double valor;
	private int sillasMax;
	
	private SeparateChainingHashST<String, ListaEncadenada<Datos>> datos;
	private ListaEncadenada<Datos> datosAerolineas;
	
	public Datos ( ){	
	}
	
	public Datos (String aerolinea, int numeroVuelo, String origen, String destino, Time horaSalida, Time horaLlegada, String avion, int numSillas, boolean[] dias, boolean tipoVuelo){
		this.aerolinea = aerolinea;
		this.numeroVuelo = numeroVuelo;
		this.origen = origen;
		this.destino = destino;
		this.horaSalida = horaSalida;
		this.horaLlegada = horaLlegada;
		this.avion = avion;
		this.numSillas = numSillas;
		this.dias = dias;
		this.tipoVuelo = tipoVuelo;
	}
	
	
	public Datos (String aerolinea, double valor, int sillasMax){
		this.aerolinea = aerolinea;
		this.valor = valor;
		this.sillasMax = sillasMax;
	}
	
	public String darAerolinea ( ){
		return aerolinea;
	}
	
	public int darNumeroVuelo ( ){
		return numeroVuelo;
	}
	
	public String darOrigen ( ){
		return origen;
	}
	
	public String darDestino ( ){
		return destino;
	}
	
	public Time darHoraSalida ( ){
		return horaSalida;
	}
	
	public Time darHoraLlegada ( ){
		return horaLlegada;
	}
	
	public String darTipoAvion ( ){
		return avion;
	}
	
	public int darNumeroSillas ( ){
		return numSillas;
	}
	
	public boolean[] darDiasVuelo ( ){
		return dias;
	}
	
	public boolean darTipoVuelo ( ){
		return tipoVuelo;
	}
	
	public SeparateChainingHashST<String, ListaEncadenada<Datos>> darDatos ( ){
		return datos;
	}
	
	public ListaEncadenada<Datos> darDatosAerolineas ( ){
		return datosAerolineas;
	}
	
	public void setLista (SeparateChainingHashST<String, ListaEncadenada<Datos>> lista){
		datos = lista;
	}
	
	public void setListaAerolineas (ListaEncadenada<Datos> lista){
		datosAerolineas = lista;
	}
	
	public double darValorMinuto ( ){
		return valor;
	}
	
	public int darSillasMax ( ){
		return sillasMax;
	}
	
	public void imprimirVuelos ( ){
		
		Iterator<String> iter = datos.keys().iterator();
		while (iter.hasNext()){
			String n = iter.next();
			ListaEncadenada<Datos> datoActual = datos.get(n);
			for (Datos actual: datoActual){
				boolean[] dias = actual.darDiasVuelo();
				String v = ""; String tipo = "Internacional";
				for (int i = 0; i < dias.length; i++){
					if (dias[i]){
						v = v + "x ";
					} else {
						v = v + "  ";
					}
				}
				if (actual.darTipoVuelo()){
					tipo = "Nacional";
				}
				System.out.println(actual.darAerolinea() + " " + actual.darNumeroVuelo() + " " + actual.darOrigen() + " " + actual.darDestino() + " " + actual.darHoraSalida() + " " + actual.darHoraLlegada() + " " + actual.darTipoAvion() + " " + actual.darNumeroSillas() + v + tipo);
			}
		}
	}
}
